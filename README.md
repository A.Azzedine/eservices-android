# NbaUpdates

Projet réalisé par **Azzedine AIT ELHAJ**, M2 eservices (FA).

## Présentation de l’application NbaUpdates

D’Octobre à Avril se tient la saison régulière NBA. Durant celle-ci, (quasiment) tous les jours ont
lieu 2 à 10 matchs entre les équipes de cette ligue. Il est donc difficile de regarder tous les
matchs quotidiennement. De plus tous les matchs ne sont pas intéressant. Cependant, il est
plaisant pour un fan de NBA de suivre les résultats régulièrement pour avoir une idée du
classement et de l’évolution de chaque équipe.


L’objectif de l’application est de permettre à ses utilisateurs de consulter la liste des matchs de la
nuit et s’ils le désirent, voir les scores de ces matchs. NbaUpdates permet aussi de sauvegarder
des matchs dans une liste de favoris.


L’idée derrière cette application est d’offrir aux fans de NBA l’opportunité de consulter la liste des
matchs NBA de la nuit. Ils pourront ainsi décider des matchs pour lesquels il souhaitent regarder
un replay. Pour les autres matchs, il pourront consulter les scores directement dans l’application
afin de se tenir au courant des résultats de chaque équipe. Si un match les intéressent mais qu’ils
ne peuvent pas le regarder dans la journée, ils peuvent l’ajouter à leurs favoris afin de se
rappeler plus tard qu’ils voulaient regarder ce match.


