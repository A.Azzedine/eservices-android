package eservices.android.nbaupdates;

import android.os.Bundle;

import com.facebook.stetho.Stetho;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import eservices.android.nbaupdates.data.db.GamePreviewDatabase;
import eservices.android.nbaupdates.data.di.FakeDependencyInjection;
import eservices.android.nbaupdates.data.entity.GamePreviewActionInterface;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import eservices.android.nbaupdates.data.repository.gameDisplay.local.GamePreviewDisplayLocalDataSource;
import eservices.android.nbaupdates.ui.main.GameDisplayAdapter;
import eservices.android.nbaupdates.ui.main.SectionsPagerAdapter;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity{

    private Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        Stetho.initializeWithDefaults(this);
        FakeDependencyInjection.setContext(getBaseContext());
    }


}