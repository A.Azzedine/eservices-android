package eservices.android.nbaupdates.data.api;

import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GameService {

    @Headers({
            "x-rapidapi-host: api-nba-v1.p.rapidapi.com",
            "x-rapidapi-key: cdc232a75dmsh3882461eab7e7f5p1d99d7jsne23fe9a3a2ab"
    })
    @GET("gameDetails/{gameid}")
    Single<GameDetailResponse>  gameDetail(@Path("gameid") String gameid);

    @Headers({
            "x-rapidapi-host: api-nba-v1.p.rapidapi.com",
            "x-rapidapi-key: cdc232a75dmsh3882461eab7e7f5p1d99d7jsne23fe9a3a2ab"
    })
    @GET("games/date/{date}")
    Single<GameDetailResponse>  gameDetailByDate(@Path("date") String date);
}
