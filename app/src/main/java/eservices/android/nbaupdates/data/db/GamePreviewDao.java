package eservices.android.nbaupdates.data.db;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface GamePreviewDao {

    @Query("SELECT * from gamepreviewentity")
    Flowable<List<GamePreviewEntity>> loadFavorites();

    @Insert
    public Completable addGamePreviewToFavorites(GamePreviewEntity bookEntity);

    @Query("DELETE FROM gamepreviewentity WHERE id = :id")
    public Completable deleteGamePreviewFromFavorites(String id);

    @Query("SELECT id from gamepreviewentity")
    Single<List<String>> getFavoriteIdList();

    @Query("SELECT count() from gamepreviewentity WHERE id= :id")
    Single<Integer> getFavoriteCountById(String id);

}
