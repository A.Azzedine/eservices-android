package eservices.android.nbaupdates.data.repository.gameDisplay;

import java.util.List;

import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import eservices.android.nbaupdates.data.repository.gameDisplay.local.GamePreviewDisplayLocalDataSource;
import eservices.android.nbaupdates.data.repository.gameDisplay.remote.GamePreviewDisplayRemoteDataSource;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class GamePreviewDisplayDataRepository implements GamePreviewDisplayRepository {

    private GamePreviewDisplayLocalDataSource gamePreviewDisplayLocalDataSource;
    private GamePreviewDisplayRemoteDataSource gamePreviewDisplayRemoteDataSource;

    public GamePreviewDisplayDataRepository(GamePreviewDisplayLocalDataSource gamePreviewDisplayLocalDataSource, GamePreviewDisplayRemoteDataSource gamePreviewDisplayRemoteDataSource){
        this.gamePreviewDisplayLocalDataSource = gamePreviewDisplayLocalDataSource;
        this.gamePreviewDisplayRemoteDataSource = gamePreviewDisplayRemoteDataSource;
    }

    @Override
    public Flowable<List<GamePreviewEntity>> getFavoriteGamePreview() {
        return gamePreviewDisplayLocalDataSource.loadFavorites();
    }

    @Override
    public Completable addFavoriteGamePreview(GamePreviewEntity gpe){
        return gamePreviewDisplayLocalDataSource.addGamePreviewToFavorites(gpe);
    }

    public Completable deleteGamePreviewFromFavorites(String id) {
        return gamePreviewDisplayLocalDataSource.deleteGamePreviewFromFavorites(id);
    }

    @Override
    public Single<Integer> getFavoriteCountById(String id){
        return gamePreviewDisplayLocalDataSource.getFavoriteCountById(id);
    }

    @Override
    public Single<GameDetailResponse> getGameDisplayResponseByDate(String date) {
        return gamePreviewDisplayRemoteDataSource.getGameDetailResponseByDate(date);
    }

}
