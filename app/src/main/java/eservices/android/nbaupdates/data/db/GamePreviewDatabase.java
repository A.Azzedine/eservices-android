package eservices.android.nbaupdates.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import eservices.android.nbaupdates.data.entity.GamePreviewEntity;

@Database(entities = {GamePreviewEntity.class}, version = 1)
public abstract class GamePreviewDatabase extends RoomDatabase {
    public abstract GamePreviewDao gamePreviewDao();
}
