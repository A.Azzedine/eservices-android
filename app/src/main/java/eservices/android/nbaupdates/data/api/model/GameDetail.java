package eservices.android.nbaupdates.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

public class GameDetail {
    @SerializedName("gameId")
    public String id;

    @SerializedName("vTeam")
    public TeamInfo visitorTeam;

    @SerializedName("hTeam")
    public TeamInfo homeTeam;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TeamInfo getVisitorTeam() {
        return visitorTeam;
    }

    public void setVisitorTeam(TeamInfo visitorTeam) {
        this.visitorTeam = visitorTeam;
    }

    public TeamInfo getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(TeamInfo homeTeam) {
        this.homeTeam = homeTeam;
    }
}
