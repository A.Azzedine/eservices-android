package eservices.android.nbaupdates.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponse {

    @SerializedName("game")
    List<GameDetail> gde;

    @SerializedName("games")
    List<GameDetail> gameList;

    public GameDetail getGde() {
        return gde.get(0);
    }

    public List<GameDetail> getGameList() {return gameList;}

}
