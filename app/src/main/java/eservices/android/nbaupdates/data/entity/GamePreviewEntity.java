package eservices.android.nbaupdates.data.entity;

import java.util.List;

import androidx.room.* ;
import androidx.annotation.* ;

@Entity
public class GamePreviewEntity {

    @NonNull
    @PrimaryKey
    public String id;

    public String visitor_team_name;
    public String home_team_name;
    public String visitor_team_score;
    public String home_team_score;
    public String visitor_team_logo_url;
    public String home_team_logo_url;


    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getVisitor_team_name() {
        return visitor_team_name;
    }

    public void setVisitor_team_name(String visitor_team_name) {
        this.visitor_team_name = visitor_team_name;
    }

    public String getHome_team_name() {
        return home_team_name;
    }

    public void setHome_team_name(String home_team_name) {
        this.home_team_name = home_team_name;
    }

    public String getVisitor_team_score() {
        return ""+visitor_team_score;
    }

    public void setVisitor_team_score(String visitor_team_score) {
        this.visitor_team_score = visitor_team_score;
    }

    public String getHome_team_score() {
        return ""+home_team_score;
    }

    public void setHome_team_score(String home_team_score) {
        this.home_team_score = home_team_score;
    }

    public String getVisitor_team_logo_url() {
        return visitor_team_logo_url;
    }

    public void setVisitor_team_logo_url(String visitor_team_logo_url) {
        this.visitor_team_logo_url = visitor_team_logo_url;
    }

    public String getHome_team_logo_url() {
        return home_team_logo_url;
    }

    public void setHome_team_logo_url(String home_team_logo_url) {
        this.home_team_logo_url = home_team_logo_url;
    }
}
