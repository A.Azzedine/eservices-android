package eservices.android.nbaupdates.data.api.model;

import com.google.gson.annotations.SerializedName;

public class TeamInfo {

    //When asking /gameDetails/
    @SerializedName("nickname")
    public String name;

    //When asking /games/date/
    @SerializedName("nickName")
    public String nickname;

    @SerializedName("teamId")
    public String id;
    @SerializedName("shortName")
    public String shortname;
    @SerializedName("score")
    public TeamScores scores;
    @SerializedName("logo")
    public String logoUrl;


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public TeamScores getScores() {
        return scores;
    }

    public void setScores(TeamScores scores) {
        this.scores = scores;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
}
