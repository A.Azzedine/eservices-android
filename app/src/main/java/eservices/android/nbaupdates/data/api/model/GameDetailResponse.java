package eservices.android.nbaupdates.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GameDetailResponse {

    @SerializedName("api")
    ApiResponse api;



    public GameDetail getGameDetail(){
        return api.getGde();
    }
    public List<GameDetail> getGameList(){
        return api.getGameList();
    }
}
