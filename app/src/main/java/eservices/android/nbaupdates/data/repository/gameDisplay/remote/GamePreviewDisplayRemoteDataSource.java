package eservices.android.nbaupdates.data.repository.gameDisplay.remote;

import eservices.android.nbaupdates.data.api.GameService;
import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import io.reactivex.Single;

public class GamePreviewDisplayRemoteDataSource {

    private GameService gameService;

    public GamePreviewDisplayRemoteDataSource(GameService gs){ this.gameService = gs;}

    public Single<GameDetailResponse> getGameDetailResponseByDate(String date){
        return gameService.gameDetailByDate(date);
    }
}
