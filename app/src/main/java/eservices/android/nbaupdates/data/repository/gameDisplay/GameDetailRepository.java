package eservices.android.nbaupdates.data.repository.gameDisplay;

import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import io.reactivex.Single;

public interface GameDetailRepository {

    Single<GameDetailResponse> getGameDisplayResponse(String gameId);

}
