package eservices.android.nbaupdates.data.repository.gameDisplay.mapper;

import android.util.Log;

import eservices.android.nbaupdates.data.api.model.GameDetail;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;

public class GameDetailToGamePreviewEntityMapper {

    public GamePreviewEntity map(GameDetail gd){
        GamePreviewEntity gpe = new GamePreviewEntity();
        gpe.setId(gd.getId());
        gpe.setHome_team_name(gd.getHomeTeam().getNickname());
        gpe.setHome_team_logo_url(gd.getHomeTeam().getLogoUrl());
        gpe.setHome_team_score(gd.getHomeTeam().getScores().getPoints());
        gpe.setVisitor_team_name(gd.getVisitorTeam().getNickname());
        gpe.setVisitor_team_logo_url(gd.getVisitorTeam().getLogoUrl());
        gpe.setVisitor_team_score(gd.getVisitorTeam().getScores().getPoints());
        return gpe;
    }

}
