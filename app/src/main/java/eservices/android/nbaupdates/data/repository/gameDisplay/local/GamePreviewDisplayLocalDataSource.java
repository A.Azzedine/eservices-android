package eservices.android.nbaupdates.data.repository.gameDisplay.local;


import java.util.List;

import eservices.android.nbaupdates.data.db.GamePreviewDatabase;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class GamePreviewDisplayLocalDataSource {

    private GamePreviewDatabase bookDatabase;

    public GamePreviewDisplayLocalDataSource(GamePreviewDatabase bookDatabase) {
        this.bookDatabase = bookDatabase;
    }

    public Flowable<List<GamePreviewEntity>> loadFavorites() {
        return bookDatabase.gamePreviewDao().loadFavorites();
    }

    public Completable addGamePreviewToFavorites(GamePreviewEntity gamePreviewEntity) {
        return bookDatabase.gamePreviewDao().addGamePreviewToFavorites(gamePreviewEntity);
    }

    public Completable deleteGamePreviewFromFavorites(String id) {
        return bookDatabase.gamePreviewDao().deleteGamePreviewFromFavorites(id);
    }

    public Single<List<String>> getFavoriteIdList() {
        return bookDatabase.gamePreviewDao().getFavoriteIdList();
    }

    public Single<Integer> getFavoriteCountById(String id){
        return bookDatabase.gamePreviewDao().getFavoriteCountById(id);
    }

}
