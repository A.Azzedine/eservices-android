package eservices.android.nbaupdates.data.api.model;

import java.util.List;

public class TeamScores {

    public String win;
    public String loss;
    public String points;
    public List<String> linescore;


    public String getWin() {
        return win;
    }

    public void setWin(String win) {
        this.win = win;
    }

    public String getLoss() {
        return loss;
    }

    public void setLoss(String loss) {
        this.loss = loss;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public List<String> getLinescore() {
        return linescore;
    }

    public void setLinescore(List<String> linescore) {
        this.linescore = linescore;
    }
}
