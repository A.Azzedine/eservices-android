package eservices.android.nbaupdates.data.entity;

public interface GamePreviewActionInterface {


    void onGameClicked(String gameId);
    void onAddFavorite(GamePreviewEntity gpe);
    void onRemoveFavorite(GamePreviewEntity gpe);

}
