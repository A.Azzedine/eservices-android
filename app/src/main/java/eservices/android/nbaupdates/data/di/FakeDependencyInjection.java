package eservices.android.nbaupdates.data.di;

import android.content.Context;


import androidx.room.Room;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import eservices.android.nbaupdates.data.api.GameService;
import eservices.android.nbaupdates.data.db.GamePreviewDatabase;
import eservices.android.nbaupdates.data.repository.gameDisplay.GameDetailDataRepository;
import eservices.android.nbaupdates.data.repository.gameDisplay.GameDetailRepository;
import eservices.android.nbaupdates.data.repository.gameDisplay.GamePreviewDisplayDataRepository;
import eservices.android.nbaupdates.data.repository.gameDisplay.local.GamePreviewDisplayLocalDataSource;
import eservices.android.nbaupdates.data.repository.gameDisplay.remote.GameDetailRemoteDataSource;
import eservices.android.nbaupdates.data.repository.gameDisplay.remote.GamePreviewDisplayRemoteDataSource;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Please never do that in a production app. Ever.
 * For the purpose of our course, this is the best option to cover interesting topics as
 * we don't have time to dig into Dependency Injection frameworks such as the famous Dagger.
 * Singleton are compulsory for some classes, such as the one here. If you don't know why, then ask me.
 * Note that this god object doesn't handle Scopes nor component lifecycles so this really shouldn't be
 * the way to go when you master the craft of your software.
 */
public class FakeDependencyInjection {


    private static Retrofit retrofit;
    private static Gson gson;
    private static GamePreviewDatabase gamePreviewDatabase;
    private static GamePreviewDisplayLocalDataSource gamePreviewDisplayLocalDataSource;
    private static GamePreviewDisplayRemoteDataSource gamePreviewDisplayRemoteDataSource;
    private static GamePreviewDisplayDataRepository gamePreviewRepository;
    private static GameService gameDetailService;
    private static GameDetailRepository gameDetailRepository;
    private static Context applicationContext;


    public static GamePreviewDisplayLocalDataSource getGamePreviewDisplayLocalDataSource(){
        if(gamePreviewDisplayLocalDataSource == null){
            gamePreviewDisplayLocalDataSource = new GamePreviewDisplayLocalDataSource(getGamePreviewDatabase());
        }
        return gamePreviewDisplayLocalDataSource;
    }

    public static GamePreviewDisplayRemoteDataSource getGamePreviewDisplayRemoteDataSource(){
        if(gamePreviewDisplayRemoteDataSource == null){
            gamePreviewDisplayRemoteDataSource = new GamePreviewDisplayRemoteDataSource(getGameDetailService());
        }
        return gamePreviewDisplayRemoteDataSource;
    }



    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api-nba-v1.p.rapidapi.com/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();
        }
        return retrofit;
    }

    public static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static void setContext(Context context) {
        applicationContext = context;
    }

    public static GamePreviewDatabase getGamePreviewDatabase() {
        if (gamePreviewDatabase == null) {
            gamePreviewDatabase = Room.databaseBuilder(applicationContext,
                    GamePreviewDatabase.class, "game-preview-database").build();
        }
        return gamePreviewDatabase;
    }

    public static GameDetailRepository getGameDetailRepository(){
        if(gameDetailRepository == null){
            gameDetailRepository = new GameDetailDataRepository(new GameDetailRemoteDataSource(getGameDetailService()));
        }
        return gameDetailRepository;
    }

    public static GamePreviewDisplayDataRepository getGamePreviewRepository(){
        if(gamePreviewRepository == null){
            gamePreviewRepository = new GamePreviewDisplayDataRepository(getGamePreviewDisplayLocalDataSource(), getGamePreviewDisplayRemoteDataSource());
        }
        return gamePreviewRepository;

    }


    public static GameService getGameDetailService(){
        if(gameDetailService == null){
            gameDetailService = getRetrofit().create(GameService.class);
        }
        return gameDetailService;
    }


}

