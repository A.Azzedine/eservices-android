package eservices.android.nbaupdates.data.repository.gameDisplay;

import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import eservices.android.nbaupdates.data.repository.gameDisplay.remote.GameDetailRemoteDataSource;
import io.reactivex.Single;

public class GameDetailDataRepository implements GameDetailRepository {
    private GameDetailRemoteDataSource gameDetailRemoteDataSource;

    public GameDetailDataRepository(GameDetailRemoteDataSource gameDetailRemoteDataSource){
        this.gameDetailRemoteDataSource = gameDetailRemoteDataSource;
    }

    @Override
    public Single<GameDetailResponse> getGameDisplayResponse(String gameId) {
        return gameDetailRemoteDataSource.getGameDetailResponse(gameId);
    }


}
