package eservices.android.nbaupdates.data.repository.gameDisplay;

import java.util.List;

import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface GamePreviewDisplayRepository {

    public Flowable<List<GamePreviewEntity>> getFavoriteGamePreview();

    public Completable addFavoriteGamePreview(GamePreviewEntity gpe);

    public Completable deleteGamePreviewFromFavorites(String id);

    public Single<Integer> getFavoriteCountById(String id);

    Single<GameDetailResponse> getGameDisplayResponseByDate(String date);


}
