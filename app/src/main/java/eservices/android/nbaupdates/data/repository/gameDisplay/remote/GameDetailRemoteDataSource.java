package eservices.android.nbaupdates.data.repository.gameDisplay.remote;

import eservices.android.nbaupdates.data.api.GameService;
import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import io.reactivex.Single;

public class GameDetailRemoteDataSource {

    private GameService gameService;

    public GameDetailRemoteDataSource(GameService gs){
        this.gameService = gs;
    }

    public Single<GameDetailResponse> getGameDetailResponse(String gameid){
        return gameService.gameDetail(gameid);
    }


}
