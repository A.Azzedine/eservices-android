package eservices.android.nbaupdates.ui.main;

import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.ArrayList;
import java.util.List;

import eservices.android.nbaupdates.R;
import eservices.android.nbaupdates.data.di.FakeDependencyInjection;
import eservices.android.nbaupdates.data.entity.GamePreviewActionInterface;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class GameDisplayAdapter extends RecyclerView.Adapter<GameDisplayAdapter.GenericViewHolder> {

    public static int VIEW_CLASSIC = 1;
    public static int VIEW_CONDENSATE = 2;

    public static abstract class GenericViewHolder extends RecyclerView.ViewHolder
    {
        public GenericViewHolder(View itemView) {
            super(itemView);
        }

        abstract void bind(GamePreviewEntity gameViewModel, int position);
    }

    public static class GameDisplayViewHolder extends GenericViewHolder  {
        private TextView h_team_name;
        private TextView v_team_name;
        private ImageButton h_team_logo;
        private ImageButton v_team_logo;
        private View v;
        private GamePreviewEntity gameViewModel;
        private int position;
        private GamePreviewActionInterface gameActionInterface;


        public GameDisplayViewHolder(View v, final GamePreviewActionInterface gameActionInterface) {
            super(v);
            this.v = v;

            h_team_name = v.findViewById(R.id.h_team_name);
            h_team_logo = v.findViewById(R.id.h_team_logo);
            v_team_name = v.findViewById(R.id.v_team_name);
            v_team_logo = v.findViewById(R.id.v_team_logo);
            this.gameActionInterface = gameActionInterface;
            setupListeners();

        }

        private void setupListeners(){

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gameActionInterface.onGameClicked(gameViewModel.id);
                }
            });
            v_team_logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gameActionInterface.onGameClicked(gameViewModel.id);
                }
            });
            h_team_logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gameActionInterface.onGameClicked(gameViewModel.id);
                }
            });
            final ImageButton favoriteButton =  v.findViewById(R.id.favorite_button);
            favoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FakeDependencyInjection.getGamePreviewRepository().getFavoriteCountById(gameViewModel.getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new SingleObserver<Integer>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Integer count) {
                                    if(count >= 1){
                                        gameActionInterface.onRemoveFavorite(gameViewModel);
                                        favoriteButton.setImageResource(android.R.drawable.btn_star_big_off);
                                    }else{
                                        gameActionInterface.onAddFavorite(gameViewModel);
                                        favoriteButton.setImageResource(android.R.drawable.btn_star_big_on);
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                }
            });
        }

        void bind(GamePreviewEntity gameViewModel, int position) {

            this.gameViewModel = gameViewModel;
            this.position = position;
            h_team_name.setText(gameViewModel.getHome_team_name());
            Glide.with(v)
                    .load(gameViewModel.getHome_team_logo_url())
                    .fitCenter()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(h_team_logo);
            v_team_name.setText(gameViewModel.getVisitor_team_name());
            Glide.with(v)
                    .load(gameViewModel.getVisitor_team_logo_url())
                    .fitCenter()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(v_team_logo);
            final ImageButton favoriteButton =  v.findViewById(R.id.favorite_button);
            setFavButton(favoriteButton);

        }


        public void setFavButton(final ImageButton favoriteButton){
            FakeDependencyInjection.getGamePreviewRepository().getFavoriteCountById(gameViewModel.getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new SingleObserver<Integer>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(Integer count) {
                            if(count >= 1){
                                favoriteButton.setImageResource(android.R.drawable.btn_star_big_on);
                            }else{
                                favoriteButton.setImageResource(android.R.drawable.btn_star_big_off);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }
                    });
        }


    }

    public static class GameDisplayCondensateViewHolder extends GenericViewHolder  {
        private ImageButton h_team_logo;
        private ImageButton v_team_logo;
        private View v;
        private GamePreviewEntity gameViewModel;
        private int position;
        private GamePreviewActionInterface gameActionInterface;


        public GameDisplayCondensateViewHolder(View v, final GamePreviewActionInterface gameActionInterface) {
            super(v);
            this.v = v;
            h_team_logo = v.findViewById(R.id.h_team_logo);
            v_team_logo = v.findViewById(R.id.v_team_logo);
            this.gameActionInterface = gameActionInterface;
            setupListeners();

        }

        private void setupListeners(){

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gameActionInterface.onGameClicked(gameViewModel.id);
                }
            });
            v_team_logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gameActionInterface.onGameClicked(gameViewModel.id);
                }
            });
            h_team_logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gameActionInterface.onGameClicked(gameViewModel.id);
                }
            });
            final ImageButton favoriteButton =  v.findViewById(R.id.favorite_button);
            favoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FakeDependencyInjection.getGamePreviewRepository().getFavoriteCountById(gameViewModel.getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new SingleObserver<Integer>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Integer count) {
                                    if(count >= 1){
                                        gameActionInterface.onRemoveFavorite(gameViewModel);
                                        favoriteButton.setImageResource(android.R.drawable.btn_star_big_off);
                                    }else{
                                        gameActionInterface.onAddFavorite(gameViewModel);
                                        favoriteButton.setImageResource(android.R.drawable.btn_star_big_on);
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                }
            });
        }

        void bind(GamePreviewEntity gameViewModel, int position) {

            this.gameViewModel = gameViewModel;
            this.position = position;
            Glide.with(v)
                    .load(gameViewModel.getHome_team_logo_url())
                    .fitCenter()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(h_team_logo);
            Glide.with(v)
                    .load(gameViewModel.getVisitor_team_logo_url())
                    .fitCenter()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(v_team_logo);

            final ImageButton favoriteButton =  v.findViewById(R.id.favorite_button);
            setFavButton(favoriteButton);
        }

        public void setFavButton(final ImageButton favoriteButton){
            FakeDependencyInjection.getGamePreviewRepository().getFavoriteCountById(gameViewModel.getId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new SingleObserver<Integer>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(Integer count) {
                            if(count >= 1){
                                favoriteButton.setImageResource(android.R.drawable.btn_star_big_on);
                            }else{
                                favoriteButton.setImageResource(android.R.drawable.btn_star_big_off);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }
                    });
        }

    }

    private List<GamePreviewEntity> gameViewModelList;
    boolean condensate ;
    private GamePreviewActionInterface gameActionInterface;

    // Provide a suitable constructor (depends on the kind of dataset)
    public GameDisplayAdapter(GamePreviewActionInterface gameActionInterface, boolean condensate) {
        gameViewModelList = new ArrayList<>();
        this.condensate = condensate;
        this.gameActionInterface = gameActionInterface;
    }

    public void bindViewModels(List<GamePreviewEntity> gameViewModelList) {
        this.gameViewModelList.clear();
        this.gameViewModelList.addAll(gameViewModelList);
        notifyDataSetChanged();
    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(GenericViewHolder holder, int position) {
                holder.bind(gameViewModelList.get(position),
                        position
                );
    }

    // Create new views (invoked by the layout manager)
    // Depending on viewType will use a different adapter for normal or condensate view
    // condensate view is used in grid layout to display less information
    @Override
    public GenericViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        GenericViewHolder gameViewHolder = null;
        switch(viewType) {
            case 2:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.gamedisplaycondensate_item_recyclerview, parent, false);
                gameViewHolder = new GameDisplayCondensateViewHolder(v, gameActionInterface);
                break;
            default:
                View v2 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.gamedisplay_item_recyclerview, parent, false);
                gameViewHolder = new GameDisplayViewHolder(v2, gameActionInterface);
                break;


        }
        return gameViewHolder;

    }



    public List<GamePreviewEntity> getGameViewModelList() {
        return gameViewModelList;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return gameViewModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(condensate) {
            return VIEW_CONDENSATE;
        }else {
            return VIEW_CLASSIC;
        }

    }

}
