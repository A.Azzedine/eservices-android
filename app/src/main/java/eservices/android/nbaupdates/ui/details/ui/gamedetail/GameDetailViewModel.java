package eservices.android.nbaupdates.ui.details.ui.gamedetail;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import eservices.android.nbaupdates.data.di.FakeDependencyInjection;
import eservices.android.nbaupdates.data.api.model.GameDetail;
import io.reactivex.CompletableObserver;
import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class GameDetailViewModel extends ViewModel {
    private MutableLiveData<GameDetail> gameDetail;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LiveData<GameDetail> getGameDetail(String gameId){
        if(gameDetail == null){
            gameDetail = new MutableLiveData<GameDetail>();
            loadGameDetail(gameId);
        }
        return gameDetail;
    }

    private void loadGameDetail(String gameId) {
        compositeDisposable.clear();
        compositeDisposable.add(FakeDependencyInjection.getGameDetailRepository().getGameDisplayResponse(gameId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<GameDetailResponse>(){
                    @Override
                    public void onSuccess(GameDetailResponse gameDetailResponse) {
                        gameDetail.setValue(gameDetailResponse.getGameDetail());
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                }));

    }
}
