package eservices.android.nbaupdates.ui.details;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import eservices.android.nbaupdates.R;
import eservices.android.nbaupdates.ui.details.ui.gamedetail.GameDetailFragment;
import eservices.android.nbaupdates.ui.main.GameDisplayListFragment;

public class GameDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_detail);
        Bundle b = getIntent().getExtras();
        String gameId="-1";
        if(b != null)
            gameId = b.getString(GameDisplayListFragment.GAMEID_KEY);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, GameDetailFragment.newInstance(gameId))
                    .commitNow();
        }
    }
}
