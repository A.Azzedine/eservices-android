package eservices.android.nbaupdates.ui.main;

import android.util.Log;

import java.util.List;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import eservices.android.nbaupdates.data.api.model.GameDetail;
import eservices.android.nbaupdates.data.api.model.GameDetailResponse;
import eservices.android.nbaupdates.data.di.FakeDependencyInjection;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;

public class PageViewModel extends ViewModel {

    private MutableLiveData<List<GameDetail>> gameList;
    private MutableLiveData<List<GamePreviewEntity>> favoriteList;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LiveData<List<GameDetail>> getGameList(String date){
        if(gameList == null){
            gameList = new MutableLiveData<List<GameDetail>>();
            loadGameList(date);
        }
        return gameList;
    }

    public LiveData<List<GamePreviewEntity>> getFavoriteList(){
        if(favoriteList == null){
            favoriteList = new MutableLiveData<List<GamePreviewEntity>>();
            loadFavoriteList();
        }
        return favoriteList;
    }

    private void loadGameList(String date) {
        compositeDisposable.clear();
        compositeDisposable.add(FakeDependencyInjection.getGamePreviewRepository().getGameDisplayResponseByDate(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<GameDetailResponse>(){
                    @Override
                    public void onSuccess(GameDetailResponse gameDetailResponse) {
                        gameList.setValue(gameDetailResponse.getGameList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Games",e.getMessage());
                    }
                }));

    }

    private void loadFavoriteList(){
        compositeDisposable.clear();
        compositeDisposable.add(FakeDependencyInjection.getGamePreviewRepository().getFavoriteGamePreview()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ResourceSubscriber<List<GamePreviewEntity>>() {
                    @Override
                    public void onNext( List<GamePreviewEntity> games) {
                        favoriteList.setValue(games);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }
}