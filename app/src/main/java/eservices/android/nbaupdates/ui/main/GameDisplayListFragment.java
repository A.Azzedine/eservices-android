package eservices.android.nbaupdates.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import eservices.android.nbaupdates.R;
import eservices.android.nbaupdates.data.api.model.GameDetail;
import eservices.android.nbaupdates.data.di.FakeDependencyInjection;
import eservices.android.nbaupdates.data.entity.GamePreviewActionInterface;
import eservices.android.nbaupdates.data.entity.GamePreviewEntity;
import eservices.android.nbaupdates.data.repository.gameDisplay.mapper.GameDetailToGamePreviewEntityMapper;
import eservices.android.nbaupdates.ui.details.GameDetailActivity;
import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A placeholder fragment containing a simple view.
 */
public class GameDisplayListFragment extends Fragment implements GamePreviewActionInterface {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String LAYOUT_NAME = "layout_name";
    private static final String LAYOUT_LINEAR = "linear_layout";
    private static final String LAYOUT_GRID = "grid_layout";
    public static final String GAMEID_KEY = "gameID";

    private PageViewModel pageViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fab;
    private GameDisplayAdapter gda = null;
    private View thisView ;
    private int index;



    public static GameDisplayListFragment newInstance(int index) {
        GameDisplayListFragment fragment = new GameDisplayListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.index = index;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);

        if (getArguments() != null) {
            this.index = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        if(savedInstanceState != null){
            CharSequence layoutType = savedInstanceState.getCharSequence(LAYOUT_NAME);
            if(layoutType != null && layoutType.length() > 0){
                if(layoutType.equals(LAYOUT_GRID)){
                    layoutManager = new GridLayoutManager(getContext(), 2);
                    gda = new GameDisplayAdapter(this,true);
                }else{
                    layoutManager = new LinearLayoutManager(getContext());
                    gda = new GameDisplayAdapter(this,false);
                }
            }

        }

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        thisView = root;
        //Getting current date to request api for the games of the day
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar myCalendar = Calendar.getInstance();
        String currentDate = sdf.format(myCalendar.getTime());
        final List<GamePreviewEntity> gamePreviews = new ArrayList<GamePreviewEntity>();
        switch (index){
            case 1:
                pageViewModel.getGameList(currentDate).observe(this, new Observer<List<GameDetail>>(){
                    @Override
                    public void onChanged(List<GameDetail> gameDetailList) {
                        if(gameDetailList != null && gameDetailList.size() > 0){
                            GameDetailToGamePreviewEntityMapper mapper = new GameDetailToGamePreviewEntityMapper();
                            for (GameDetail gd: gameDetailList) {
                                //Filtering game: games with incomplete data should not be displayed. At least gameID and team ids must be present.
                                if(gd.getId() != null && gd.getVisitorTeam() != null && gd.getHomeTeam() != null && gd.getHomeTeam().getId() != null && gd.getVisitorTeam().getId() != null){
                                    gamePreviews.add(mapper.map(gd));
                                }else{
                                    Log.e("Game Previews","Didn't try to map game detail to gamePreview because gamePreview data was incomplete");
                                }
                            }
                            setupRecyclerView(gamePreviews);
                        }
                    }
                });
                break;
            case 2:
                pageViewModel.getFavoriteList().observe(this, new Observer<List<GamePreviewEntity>>(){
                    @Override
                    public void onChanged(List<GamePreviewEntity> favoriteGameList) {
                        if(favoriteGameList != null && favoriteGameList.size() > 0){
                            setupRecyclerView(favoriteGameList);
                        }
                    }
                });
                break;

        }
        return root;
    }

    private void setupRecyclerView(final List<GamePreviewEntity> gamesPreviews) {
        recyclerView = thisView.findViewById(R.id.recycler_view);
        fab = thisView.findViewById(R.id.viewSwitchButton);
        if(recyclerView != null){
            //if there is no layoutManager (from a saved bundle), using a linearlayout as default layout.
            if(layoutManager == null){
                layoutManager = new LinearLayoutManager(getContext());
            }
            recyclerView.setLayoutManager(layoutManager);
            final GamePreviewActionInterface fragment = this;
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerView.getRecycledViewPool().clear();
                    recyclerView.setLayoutManager(null);
                    recyclerView.setAdapter(null);
                    if (layoutManager instanceof GridLayoutManager) {
                        layoutManager = new LinearLayoutManager(getActivity());
                        gda = new GameDisplayAdapter(fragment,false);
                    } else {
                        layoutManager = new GridLayoutManager(getActivity(), 2);
                        gda = new GameDisplayAdapter(fragment,true);
                    }
                    recyclerView.setAdapter(gda);
                    recyclerView.setLayoutManager(layoutManager);
                    gda.bindViewModels(gamesPreviews);
                }
            });
            if(gda == null){
                gda = new GameDisplayAdapter(this,false);
            }
            recyclerView.setAdapter(gda);
            gda.bindViewModels(gamesPreviews);
        }

    }

    @Override
    public void onGameClicked(String gameId) {
        Intent intent = new Intent(getActivity(), GameDetailActivity.class);
        intent.putExtra(GAMEID_KEY, gameId);
        startActivity(intent);
    }


    @Override
    public void onAddFavorite(GamePreviewEntity gpe) {
        FakeDependencyInjection.getGamePreviewRepository().addFavoriteGamePreview(gpe)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new CompletableObserver() {
                                   @Override
                                   public void onSubscribe(Disposable d) {

                                   }

                                   @Override
                                   public void onComplete() {
                                       Log.v("favorites","completed add to favorite");
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       //Probably a bad idea to display error message in log (security issue ? )
                                       Log.e("favorites",e.getMessage());
                                   }
                               }

                );
        Snackbar.make(recyclerView, "Le match a été ajouté au favoris.", Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onRemoveFavorite(GamePreviewEntity gpe) {
        FakeDependencyInjection.getGamePreviewRepository().deleteGamePreviewFromFavorites(gpe.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new CompletableObserver() {
                                   @Override
                                   public void onSubscribe(Disposable d) {

                                   }

                                   @Override
                                   public void onComplete() {
                                       Log.v("favorites","completed remove from favorite");
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       //Probably a bad idea to display error message in log (security issue ? )
                                       Log.e("favorites",e.getMessage());
                                   }
                               }

                );
        Snackbar.make(recyclerView, "Le match a été retiré des favoris.", Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(recyclerView.getLayoutManager() instanceof LinearLayoutManager){
            outState.putCharSequence(LAYOUT_NAME,LAYOUT_LINEAR);
        }
        if(recyclerView.getLayoutManager() instanceof  GridLayoutManager){
            outState.putCharSequence(LAYOUT_NAME,LAYOUT_GRID);
        }
        //I decided not to save the list of games so i could use the fragment lifecycle as a "data refresher" (forcing an api call to get it )
        super.onSaveInstanceState(outState);

    }



}
