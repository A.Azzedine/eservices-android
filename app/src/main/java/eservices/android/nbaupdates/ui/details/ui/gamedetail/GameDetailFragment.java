package eservices.android.nbaupdates.ui.details.ui.gamedetail;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.List;

import eservices.android.nbaupdates.R;
import eservices.android.nbaupdates.data.api.model.GameDetail;
import eservices.android.nbaupdates.ui.main.GameDisplayListFragment;

public class GameDetailFragment extends Fragment {

    private GameDetailViewModel mViewModel;
    private String gameID;


    public static GameDetailFragment newInstance(String gameID) {
        GameDetailFragment gdf = new GameDetailFragment();
        gdf.gameID = gameID;
        return gdf;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.game_detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            gameID = savedInstanceState.getCharSequence(GameDisplayListFragment.GAMEID_KEY).toString();
        }
        mViewModel = ViewModelProviders.of(this).get(GameDetailViewModel.class);
        mViewModel.getGameDetail(gameID).observe(this, new Observer<GameDetail>(){
            @Override
            public void onChanged(GameDetail gameDetailEntity) {
                if(gameDetailEntity != null && gameDetailEntity.getVisitorTeam() != null && gameDetailEntity.getVisitorTeam() != null){
                    addGameDetail(gameDetailEntity);
                }

            }
        });
    }

    public void addGameDetail(GameDetail gde){
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        TextView home_team_name_view = getView().findViewById(R.id.h_team_name);
        home_team_name_view.setText(gde.getHomeTeam().getName());
        TextView visitor_team_name_view = getView().findViewById(R.id.v_team_name);
        visitor_team_name_view.setText(gde.getVisitorTeam().getName());
        TextView home_team_records_view = getView().findViewById(R.id.h_team_records);

        String htr = "("+gde.getHomeTeam().getScores().getWin()+"-"+gde.getHomeTeam().getScores().getLoss()+")";
        String vtr = "("+gde.getVisitorTeam().getScores().getWin()+"-"+gde.getVisitorTeam().getScores().getLoss()+")";
        home_team_records_view.setText(htr);
        TextView visitor_team_records_view = getView().findViewById(R.id.v_team_records);
        visitor_team_records_view.setText(vtr);
        TextView home_team_score_view = getView().findViewById(R.id.h_team_score);
        home_team_score_view.setText(""+gde.getHomeTeam().getScores().getPoints());
        TextView visitor_team_score_view = getView().findViewById(R.id.v_team_score);
        visitor_team_score_view.setText(""+gde.getVisitorTeam().getScores().getPoints());
        Glide.with(getView())
                .load(gde.getHomeTeam().getLogoUrl())
                .fitCenter()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into((ImageButton)getView().findViewById(R.id.h_team_logo));
        Glide.with(getView())
                .load(gde.getVisitorTeam().getLogoUrl())
                .fitCenter()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into((ImageButton)getView().findViewById(R.id.v_team_logo));
        LinearLayout qtNameRow = getView().findViewById(R.id.qt_name_row);
        TextView FirstColumnName = new TextView(getContext());
        FirstColumnName.setText("Equipes");
        FirstColumnName.setLayoutParams(param);
        FirstColumnName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        FirstColumnName.setTextSize(pixels);
        FirstColumnName.setTypeface(FirstColumnName.getTypeface(), Typeface.BOLD);
        qtNameRow.addView(FirstColumnName);
        if(gde.getHomeTeam().getScores() != null && gde.getVisitorTeam().getScores() != null){
            for(int i = 0; i < gde.getHomeTeam().getScores().getLinescore().size(); i++){
                TextView QtName = new TextView(getContext());
                if(i < 4){
                    QtName.setText("QT"+(i+1));
                }else{
                    QtName.setText("OT"+(i-3));
                }

                QtName.setLayoutParams(param);
                QtName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                QtName.setTextSize(pixels);
                QtName.setTypeface(QtName.getTypeface(), Typeface.BOLD);
                qtNameRow.addView(QtName);
            }

            LinearLayout hTeamQtScore = getView().findViewById(R.id.h_team_qt_score_row);
            TextView homeTeamName = new TextView(getContext());
            homeTeamName.setText(gde.getHomeTeam().getShortname());
            homeTeamName.setLayoutParams(param);
            homeTeamName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            hTeamQtScore.addView(homeTeamName);
            List<String> hscores = gde.getHomeTeam().getScores().getLinescore();
            for(int i = 0; i < hscores.size(); i++){
                TextView QtScore = new TextView(getContext());
                QtScore.setText(""+hscores.get(i));
                QtScore.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                QtScore.setLayoutParams(param);
                hTeamQtScore.addView(QtScore);
            }

            LinearLayout vTeamQtScore = getView().findViewById(R.id.v_team_qt_score_row);
            TextView visitorTeamName = new TextView(getContext());
            visitorTeamName.setText(gde.getVisitorTeam().getShortname());
            visitorTeamName.setLayoutParams(param);
            visitorTeamName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            vTeamQtScore.addView(visitorTeamName);
            List<String> vscores = gde.getVisitorTeam().getScores().getLinescore();
            for(int i = 0; i < vscores.size(); i++){
                TextView QtScore = new TextView(getContext());
                QtScore.setText(""+vscores.get(i));
                QtScore.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                QtScore.setLayoutParams(param);
                vTeamQtScore.addView(QtScore);
            }
        }



    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putCharSequence(GameDisplayListFragment.GAMEID_KEY, gameID);
        super.onSaveInstanceState(outState);

    }
}
